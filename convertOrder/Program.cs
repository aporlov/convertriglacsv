﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ConvertOrder
{
    class Program
    {
        static int Main(string[] args)
        {
//            string shablon =
//                @"Клиент       : [CodeClient]
//Получатель   : [CodeDostavki]
//Оплата       : 0
//ID заказа    : 
//Клиентский ID: [ZakazCode]
//Дата заказа  : [DataOrder] 
//Позиций      : [Rows]
//Версия EXE   : ZCONVERT
//Версия CFG   : ZCONVERT
//Статус CFG   : ZCONVERT
//Прайс-лист   : [DataOrder] 
//Комментарий  : [Comment]
//";
           
            try
            {
                Encoding encoding = Encoding.GetEncoding(1251);
                #region Проверка параметров
                if (args.Length < 2)
                {
                    System.Console.WriteLine("Convert <файл заявки> <шаблон заявки>");
                    return 1;
                }

                if (!System.IO.File.Exists(args[0]) || !System.IO.File.Exists(args[1]))
                {
                    System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + " Не найден файл заявки или файл шаблона");
                    return 1;
                }       
                #endregion
                System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + " Начинаем конвертацию. " + " " + args[0]);
                //Читаем файл заявки
                IEnumerable<string> LinesOriginalFileOrder = File.ReadAllLines(args[0], encoding);
                List<Row> rows = new List<Row>();
                Order order = new Order();
                int i=0;
                string codedostavki = "";

                foreach (var item in LinesOriginalFileOrder)
                {
                    if (string.IsNullOrEmpty(item)) continue;
                    string[] buffer = item.Split(';');
                    if (i == 0)
                    {
                        order.CodeDostavki = buffer[5];
                        order.ZakazCode = buffer[3];
                        order.Comment = "@ " + buffer[0] + "Заказ № " + buffer[3] + "@ "+ buffer[4] ;
                        order.DataOrder = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                    }
                    if(i>1)
                    {
                        rows.Add(new Row() { Code = buffer[3] + "$CLI$" + order.ZakazCode, Count = buffer[2]});
                    }
                    i++;
                }
                    order.rows = rows;
                    order.Rows = rows.Count.ToString();
                //формируем заявку СИА
                string[] shablon = File.ReadAllLines(args[1], encoding);
                StringBuilder ordersia = new StringBuilder();
                ordersia.Append(string.Join("\r\n", shablon).Replace("[CodeDostavki]", order.CodeDostavki)
                                               .Replace("[CodeClient]", order.CodeClient)
                                               .Replace("[DataOrder]", order.DataOrder)
                                               .Replace("[Rows]", order.Rows)
                                               .Replace("[Comment]", order.Comment)
                                               .Replace("[CodeZakaz]", order.ZakazCode)).AppendLine();
                foreach(var item in order.rows)
                {
                    ordersia.Append(new string(' ', 50 - item.Code.Length)).Append(item.Code).Append(new string(' ', 10 - item.Count.Length)).Append(item.Count).AppendLine();                   
                }
                //пишем в файл 
                File.WriteAllText(args[0]+".txt", ordersia.ToString(), encoding);

            }
            catch (Exception e)
            {
                System.Console.WriteLine(DateTime.Now.ToString("dd:MM:yyyy hh:mm:ss") + e.Message);
                return 1;
            }

            return 0;
        }
        public class Order
        {
            public string CodeClient { get; set; }
            public string CodeDostavki { get; set; }
            public string AddressDostavki { get; set; }
            public string Rows { get; set; }
            public string DataOrder { get; set; }
            public string TimeOrder { get; set; }
            public string DataPrice { get; set; }
            public string TimePrice { get; set; }
            public string Comment { get; set; }
            public string ZakazCode { get; set; }
            public List<Row> rows { get; set; }
        }
        public class Row
        {
            public string Code { get; set; }
            public string Count { get; set; }
            public string CodeDostavkiFarm { get; set; }
        }
        public class Cod
        {
            public string CodeDostavki { get; set; }
            public string CodeClient { get; set; }
        }

    }
}
